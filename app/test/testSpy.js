//test con Spy simulando un valor

var volumen = new Volumen();

describe("Devuelve un valor falso para area cuadrado", function() {
  var area, calcVolumen, testAreaCuadrado, calcArea;

  beforeEach(function(){
    testAreaCuadrado = {
      areaCuadrado: function(lado){
        return area;
      }
    };

    spyOn(testAreaCuadrado, "areaCuadrado").and.returnValue(3);

    calcArea = testAreaCuadrado.areaCuadrado(5);
    calcVolumen = volumen.volumenCuadrado(5, testAreaCuadrado);
  });

  it("Comprobar que ha sido llamado", function() {
    expect(testAreaCuadrado.areaCuadrado).toHaveBeenCalled();
  });

  it("Devolver el valor esperado", function(){
    expect(calcArea).toEqual(3);
  });

  it("Calcular resultado de volumen", function(){
    expect(calcVolumen).toEqual(15);
  })
});
