var volumen = new Volumen();

describe("Devuelve un valor falso para area circulo", function() {
  var area, calcVolumen, testAreaCirculo, calcArea;


    testAreaCirculo = {
      areaCirculo: function(lado){
        return 3;
      }
    };

    calcArea = testAreaCirculo.areaCirculo(5);
    calcVolumen = volumen.volumenCirculo(5, testAreaCirculo);

  it("Devolver el valor esperado", function(){
    expect(calcArea).toEqual(3);
  });

  it("Calcular resultado de volumen", function(){
    expect(calcVolumen).toEqual(19.950000000000003);
  });
});
