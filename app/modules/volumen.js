function Volumen () {
	this.volumenCuadrado = function (lado, oArea){
		return oArea.areaCuadrado(lado) * lado;
	};

	this.volumenCirculo = function (radio, oArea){
		return oArea.areaCirculo(radio) * radio * ((4 / 3).toFixed(2));
	};
};

vol = new Volumen();
area = new Area();

function alertarVolumenCuadrado(){
	$('#resultCuadrado').text("El volumen es: " +vol.volumenCuadrado($('#lado').val(), area));
}

function alertarVolumenCirculo(){
	$('#resultCirculo').text("El volumen es: " +vol.volumenCirculo($('#radio').val(), area));
}
